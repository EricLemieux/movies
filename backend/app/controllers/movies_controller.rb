class MoviesController < ApplicationController
  def index
    @movies = Movie.all
    json_response(@movies)
  end

  def create
    @movie = Movie.create!(movie_params)
    json_response(@movie, :created)
  end

  def show
    @movie = Movie.find(params['id'])
    json_response(@movie)
  end

  private

  def movie_params
    params.permit(:name, :poster, :description)
  end
end
